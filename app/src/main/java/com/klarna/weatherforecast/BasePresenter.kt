package com.klarna.weatherforecast

interface BasePresenter<T> {
    fun start()
    var view: T
}