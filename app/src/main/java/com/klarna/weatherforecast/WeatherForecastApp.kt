package com.klarna.weatherforecast

import android.app.Application
import com.facebook.stetho.Stetho
import com.klarna.weatherforecast.di.modules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class WeatherForecastApp : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
        startKoin {
            androidContext(this@WeatherForecastApp)
            modules(modules)
        }
    }
}