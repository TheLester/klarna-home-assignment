package com.klarna.weatherforecast.di

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.android.gms.location.LocationServices
import com.klarna.weatherforecast.data.WeatherDataRepository
import com.klarna.weatherforecast.data.source.WeatherDataSource
import com.klarna.weatherforecast.data.source.remote.DarkSkyAPI
import com.klarna.weatherforecast.data.source.remote.WeatherRemoteDataSource
import com.klarna.weatherforecast.di.Properties.API_KEY
import com.klarna.weatherforecast.di.Properties.API_URL
import com.klarna.weatherforecast.forecast.ForecastContract
import com.klarna.weatherforecast.forecast.ForecastFragment
import com.klarna.weatherforecast.forecast.ForecastPresenter
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Properties {
    const val API_URL = "https://api.darksky.net/"
    const val API_KEY = "80a0380eef480c4000536c09553aaa9c"
    const val ICON_URL = "https://darksky.net/images/weather-icons/"
}


val networkModule = module(override = true) {

    single {
        OkHttpClient.Builder()
            .addNetworkInterceptor(StethoInterceptor())
            .build()
    }

    single {
        Retrofit.Builder()
            .client(get())
            .baseUrl(API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(DarkSkyAPI::class.java)
    }
}

val forecastModule = module {
    single {
        WeatherRemoteDataSource(API_KEY, get()) as WeatherDataSource
    }

    single {
        WeatherDataRepository(get())
    }

    factory {
        ForecastFragment() as ForecastContract.View
    }

    factory {
        ForecastPresenter(get(), get())
    }

    single {
        LocationServices.getFusedLocationProviderClient(androidContext())
    }
}


val modules = listOf(networkModule, forecastModule)