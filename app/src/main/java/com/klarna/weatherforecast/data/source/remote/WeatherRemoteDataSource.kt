package com.klarna.weatherforecast.data.source.remote

import com.klarna.weatherforecast.data.model.Forecast
import com.klarna.weatherforecast.data.source.WeatherDataSource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WeatherRemoteDataSource(
    private val apiKey: String,
    private val api: DarkSkyAPI
) : WeatherDataSource {

    override fun getForecast(
        latitude: String,
        longitude: String,
        callback: WeatherDataSource.LoadWeatherCallback
    ) =
        api
            .getForecast(apiKey, latitude, longitude)
            .enqueue(object : Callback<Forecast> {
                override fun onFailure(call: Call<Forecast>, t: Throwable) {
                    callback.onDataNotAvailable()
                }

                override fun onResponse(call: Call<Forecast>, response: Response<Forecast>) {
                    if (response.body() != null) {
                        callback.onForecastLoaded(response.body()!!)
                    } else {
                        callback.onDataNotAvailable()
                    }
                }

            })

}