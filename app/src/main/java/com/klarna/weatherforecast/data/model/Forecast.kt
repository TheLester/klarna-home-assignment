package com.klarna.weatherforecast.data.model


data class Forecast(
    val timezone: String,
    val currently: CurrentWeather,
    val hourly: HourlyWeather,
    val daily: DailyWeather
)

data class CurrentWeather(
    val time: Long,
    val summary: String,
    val icon: String,
    val temperature: Float,
    val humidity: Float,
    val pressure: Float,
    val windSpeed: Float
)

class HourlyWeather // todo
class DailyWeather // todo