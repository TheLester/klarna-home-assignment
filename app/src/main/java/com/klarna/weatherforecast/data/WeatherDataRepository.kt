package com.klarna.weatherforecast.data

import com.klarna.weatherforecast.data.source.WeatherDataSource

/**
 * Encapsulates access to data from any data source (remote, local, etc)
 * and could contain caching logic in future
 */
class WeatherDataRepository(
    private val weatherRemoteDataSource: WeatherDataSource
) : WeatherDataSource {

    override fun getForecast(
        latitude: String,
        longitude: String,
        callback: WeatherDataSource.LoadWeatherCallback
    ) = weatherRemoteDataSource.getForecast(latitude, longitude, callback)

}