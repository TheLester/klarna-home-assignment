package com.klarna.weatherforecast.data.source.remote

import com.klarna.weatherforecast.data.model.Forecast
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface DarkSkyAPI {

    @GET("forecast/{apiKey}/{latitude},{longitude}")
    fun getForecast(
        @Path("apiKey") apiKey: String,
        @Path("latitude") latitude: String,
        @Path("longitude") longitude: String
    ): Call<Forecast>

}