package com.klarna.weatherforecast.data.source

import com.klarna.weatherforecast.data.model.Forecast

interface WeatherDataSource {


    interface LoadWeatherCallback {

        fun onForecastLoaded(forecast: Forecast)

        fun onDataNotAvailable()
    }

    fun getForecast(
        latitude: String,
        longitude: String,
        callback: LoadWeatherCallback
    )
}