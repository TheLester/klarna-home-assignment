package com.klarna.weatherforecast

interface BaseView<T> {
    val presenter: T
}
