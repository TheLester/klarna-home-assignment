package com.klarna.weatherforecast.forecast

import com.klarna.weatherforecast.BasePresenter
import com.klarna.weatherforecast.BaseView
import com.klarna.weatherforecast.data.model.Forecast

/**
 * This specifies the contract between the view and the presenter.
 */
interface ForecastContract {

    interface View : BaseView<Presenter> {
        var isActive: Boolean

        fun setLoadingIndicator(active: Boolean)

        fun showForecast(forecast: Forecast)

        fun onDataNotAvailable()
    }

    interface Presenter : BasePresenter<View> {
        fun refresh()
    }
}