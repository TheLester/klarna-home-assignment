package com.klarna.weatherforecast.forecast

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.klarna.weatherforecast.R
import com.klarna.weatherforecast.data.model.Forecast
import com.klarna.weatherforecast.di.Properties.ICON_URL
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_forecast.*
import org.koin.android.ext.android.inject

class ForecastFragment : Fragment(), ForecastContract.View {

    companion object {
        const val REQUEST_LOCATION = 42
        fun newInstance() = ForecastFragment()
    }

    override val presenter by inject<ForecastPresenter>()

    override var isActive: Boolean = false
        get() = isAdded

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestPermissions(
            arrayOf(ACCESS_COARSE_LOCATION),
            REQUEST_LOCATION
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
                presenter.view = this
                presenter.start()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_forecast, container, false)

    override fun onDataNotAvailable() {
        Toast
            .makeText(requireContext(), R.string.forecast_not_available, Toast.LENGTH_LONG)
            .show()
    }

    override fun setLoadingIndicator(active: Boolean) {
        pullToRefresh.isRefreshing = active
        infoContainer.visibility = if (active) View.GONE else View.VISIBLE
    }

    @SuppressLint("SetTextI18n")
    override fun showForecast(forecast: Forecast) {
        //todo: split information between different textViews

        val summary = forecast.currently.summary
        val temperature = forecast.currently.temperature.toString()
        val humidity = forecast.currently.humidity.toString()
        val pressure = forecast.currently.pressure.toString()
        val windSpeed = forecast.currently.windSpeed.toString()

        description.text =
            "$summary \n" +
                    "Temperature: $temperature F\n" +
                    "Humidity: $humidity \n" +
                    "Pressure: $pressure \n" +
                    "Wind speed: $windSpeed"
        Picasso
            .get()
            .load(ICON_URL + forecast.currently.icon + ".png")
            .into(icon)
    }
}