package com.klarna.weatherforecast.forecast

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.klarna.weatherforecast.R

class ForecastActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forecast)

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.contentFrame, ForecastFragment.newInstance())
                .commit()
        }
    }
}
