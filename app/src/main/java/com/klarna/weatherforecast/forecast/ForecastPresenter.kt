package com.klarna.weatherforecast.forecast

import android.location.Location
import android.util.Log.e
import com.google.android.gms.location.FusedLocationProviderClient
import com.klarna.weatherforecast.data.WeatherDataRepository
import com.klarna.weatherforecast.data.model.Forecast
import com.klarna.weatherforecast.data.source.WeatherDataSource

class ForecastPresenter(
    private val forecastRepository: WeatherDataRepository,
    private val locationProviderClient: FusedLocationProviderClient
) : ForecastContract.Presenter {

    override lateinit var view: ForecastContract.View

    override fun start() {

        locationProviderClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                location?.let {
                    getForecast(location.latitude.toString(), location.longitude.toString())
                }
            }.addOnFailureListener {
                e("AAA",it.message)
            }
    }

    private fun getForecast(latitude: String, longitude: String) {
        view.setLoadingIndicator(true)

        forecastRepository
            .getForecast(latitude, longitude,
                object : WeatherDataSource.LoadWeatherCallback {

                    override fun onForecastLoaded(forecast: Forecast) {
                        view.setLoadingIndicator(false)
                        view.showForecast(forecast)
                    }

                    override fun onDataNotAvailable() {
                        view.setLoadingIndicator(false)
                        view.onDataNotAvailable()
                    }
                }
            )
    }

    override fun refresh() {
        //todo: re-request for forecast
    }
}